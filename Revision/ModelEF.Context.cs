﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Revision
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class RevisionTpEntities : DbContext
    {
        public RevisionTpEntities()
            : base("name=RevisionTpEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Enseignant> Enseignants { get; set; }
        public virtual DbSet<Etudiant> Etudiants { get; set; }
        public virtual DbSet<Inscription> Inscriptions { get; set; }
        public virtual DbSet<Matiere> Matieres { get; set; }
        public virtual DbSet<Paiement> Paiements { get; set; }
        public virtual DbSet<Seance> Seances { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
    }
}
