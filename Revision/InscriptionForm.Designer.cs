﻿namespace Revision
{
    partial class InscriptionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Ajouts = new System.Windows.Forms.GroupBox();
            this.CB_Etudiants = new System.Windows.Forms.ComboBox();
            this.CB_Matieres = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Txt_Montant = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Txt_Duree = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_Publication = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Btn_Inscription_Ajouter = new System.Windows.Forms.Button();
            this.Txt_Date_Debut = new System.Windows.Forms.TextBox();
            this.Ajouts.SuspendLayout();
            this.SuspendLayout();
            // 
            // Ajouts
            // 
            this.Ajouts.Controls.Add(this.CB_Etudiants);
            this.Ajouts.Controls.Add(this.CB_Matieres);
            this.Ajouts.Controls.Add(this.label1);
            this.Ajouts.Controls.Add(this.Txt_Montant);
            this.Ajouts.Controls.Add(this.label7);
            this.Ajouts.Controls.Add(this.label8);
            this.Ajouts.Controls.Add(this.Txt_Duree);
            this.Ajouts.Controls.Add(this.label5);
            this.Ajouts.Controls.Add(this.label6);
            this.Ajouts.Controls.Add(this.Txt_Publication);
            this.Ajouts.Controls.Add(this.label2);
            this.Ajouts.Controls.Add(this.Btn_Inscription_Ajouter);
            this.Ajouts.Controls.Add(this.Txt_Date_Debut);
            this.Ajouts.Location = new System.Drawing.Point(12, 12);
            this.Ajouts.Name = "Ajouts";
            this.Ajouts.Size = new System.Drawing.Size(417, 198);
            this.Ajouts.TabIndex = 11;
            this.Ajouts.TabStop = false;
            this.Ajouts.Text = "Ajouts";
            // 
            // CB_Etudiants
            // 
            this.CB_Etudiants.FormattingEnabled = true;
            this.CB_Etudiants.Location = new System.Drawing.Point(115, 160);
            this.CB_Etudiants.Name = "CB_Etudiants";
            this.CB_Etudiants.Size = new System.Drawing.Size(137, 21);
            this.CB_Etudiants.TabIndex = 18;
            // 
            // CB_Matieres
            // 
            this.CB_Matieres.FormattingEnabled = true;
            this.CB_Matieres.Location = new System.Drawing.Point(115, 129);
            this.CB_Matieres.Name = "CB_Matieres";
            this.CB_Matieres.Size = new System.Drawing.Size(137, 21);
            this.CB_Matieres.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 160);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "ID Etudiant :";
            // 
            // Txt_Montant
            // 
            this.Txt_Montant.Location = new System.Drawing.Point(115, 102);
            this.Txt_Montant.Name = "Txt_Montant";
            this.Txt_Montant.Size = new System.Drawing.Size(137, 20);
            this.Txt_Montant.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 135);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "ID Matiere :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 109);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Montant :";
            // 
            // Txt_Duree
            // 
            this.Txt_Duree.Location = new System.Drawing.Point(115, 48);
            this.Txt_Duree.Name = "Txt_Duree";
            this.Txt_Duree.Size = new System.Drawing.Size(137, 20);
            this.Txt_Duree.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Date publication :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Duree :";
            // 
            // Txt_Publication
            // 
            this.Txt_Publication.Location = new System.Drawing.Point(115, 74);
            this.Txt_Publication.Name = "Txt_Publication";
            this.Txt_Publication.Size = new System.Drawing.Size(137, 20);
            this.Txt_Publication.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(87, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Date Inscription :";
            // 
            // Btn_Inscription_Ajouter
            // 
            this.Btn_Inscription_Ajouter.Location = new System.Drawing.Point(312, 74);
            this.Btn_Inscription_Ajouter.Name = "Btn_Inscription_Ajouter";
            this.Btn_Inscription_Ajouter.Size = new System.Drawing.Size(75, 39);
            this.Btn_Inscription_Ajouter.TabIndex = 0;
            this.Btn_Inscription_Ajouter.Text = "Ajouter";
            this.Btn_Inscription_Ajouter.UseVisualStyleBackColor = true;
            this.Btn_Inscription_Ajouter.Click += new System.EventHandler(this.Btn_Inscription_Ajouter_Click);
            // 
            // Txt_Date_Debut
            // 
            this.Txt_Date_Debut.Location = new System.Drawing.Point(115, 21);
            this.Txt_Date_Debut.Name = "Txt_Date_Debut";
            this.Txt_Date_Debut.Size = new System.Drawing.Size(137, 20);
            this.Txt_Date_Debut.TabIndex = 4;
            // 
            // InscriptionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(455, 229);
            this.Controls.Add(this.Ajouts);
            this.Name = "InscriptionForm";
            this.Text = "InscriptionForm";
            this.Load += new System.EventHandler(this.InscriptionForm_Load);
            this.Ajouts.ResumeLayout(false);
            this.Ajouts.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Ajouts;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Txt_Montant;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txt_Duree;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txt_Publication;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Btn_Inscription_Ajouter;
        private System.Windows.Forms.TextBox Txt_Date_Debut;
        private System.Windows.Forms.ComboBox CB_Etudiants;
        private System.Windows.Forms.ComboBox CB_Matieres;
    }
}