﻿namespace Revision
{
    partial class EtudiantForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Btn_Etudiant_Lister = new System.Windows.Forms.Button();
            this.Txt_Etudiant_IDaction = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Btn_Etudiant_Supprimer = new System.Windows.Forms.Button();
            this.Btn_Etudiant_Modifier = new System.Windows.Forms.Button();
            this.Ajouts = new System.Windows.Forms.GroupBox();
            this.Txt_Etudiant_Tel = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.Txt_Etudiant_Login = new System.Windows.Forms.TextBox();
            this.Txt_Etudiant_Prenom = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Txt_Etudiant_Email = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Btn_Etudiant_Ajouter = new System.Windows.Forms.Button();
            this.Txt_Etudiant_Nom = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.Ajouts.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Btn_Etudiant_Lister);
            this.groupBox1.Controls.Add(this.Txt_Etudiant_IDaction);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.Btn_Etudiant_Supprimer);
            this.groupBox1.Controls.Add(this.Btn_Etudiant_Modifier);
            this.groupBox1.Location = new System.Drawing.Point(12, 198);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(417, 106);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Actions";
            // 
            // Btn_Etudiant_Lister
            // 
            this.Btn_Etudiant_Lister.Location = new System.Drawing.Point(311, 72);
            this.Btn_Etudiant_Lister.Name = "Btn_Etudiant_Lister";
            this.Btn_Etudiant_Lister.Size = new System.Drawing.Size(75, 23);
            this.Btn_Etudiant_Lister.TabIndex = 8;
            this.Btn_Etudiant_Lister.Text = "Lister";
            this.Btn_Etudiant_Lister.UseVisualStyleBackColor = true;
            this.Btn_Etudiant_Lister.Click += new System.EventHandler(this.Btn_Etudiant_Lister_Click);
            // 
            // Txt_Etudiant_IDaction
            // 
            this.Txt_Etudiant_IDaction.Location = new System.Drawing.Point(115, 43);
            this.Txt_Etudiant_IDaction.Name = "Txt_Etudiant_IDaction";
            this.Txt_Etudiant_IDaction.Size = new System.Drawing.Size(137, 20);
            this.Txt_Etudiant_IDaction.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(24, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "ID :";
            // 
            // Btn_Etudiant_Supprimer
            // 
            this.Btn_Etudiant_Supprimer.Location = new System.Drawing.Point(311, 43);
            this.Btn_Etudiant_Supprimer.Name = "Btn_Etudiant_Supprimer";
            this.Btn_Etudiant_Supprimer.Size = new System.Drawing.Size(75, 23);
            this.Btn_Etudiant_Supprimer.TabIndex = 2;
            this.Btn_Etudiant_Supprimer.Text = "Supprimer";
            this.Btn_Etudiant_Supprimer.UseVisualStyleBackColor = true;
            this.Btn_Etudiant_Supprimer.Click += new System.EventHandler(this.Btn_Etudiant_Supprimer_Click);
            // 
            // Btn_Etudiant_Modifier
            // 
            this.Btn_Etudiant_Modifier.Location = new System.Drawing.Point(311, 17);
            this.Btn_Etudiant_Modifier.Name = "Btn_Etudiant_Modifier";
            this.Btn_Etudiant_Modifier.Size = new System.Drawing.Size(75, 23);
            this.Btn_Etudiant_Modifier.TabIndex = 1;
            this.Btn_Etudiant_Modifier.Text = "Modifier";
            this.Btn_Etudiant_Modifier.UseVisualStyleBackColor = true;
            this.Btn_Etudiant_Modifier.Click += new System.EventHandler(this.Btn_Etudiant_Modifier_Click);
            // 
            // Ajouts
            // 
            this.Ajouts.Controls.Add(this.Txt_Etudiant_Tel);
            this.Ajouts.Controls.Add(this.label7);
            this.Ajouts.Controls.Add(this.label8);
            this.Ajouts.Controls.Add(this.Txt_Etudiant_Login);
            this.Ajouts.Controls.Add(this.Txt_Etudiant_Prenom);
            this.Ajouts.Controls.Add(this.label5);
            this.Ajouts.Controls.Add(this.label6);
            this.Ajouts.Controls.Add(this.Txt_Etudiant_Email);
            this.Ajouts.Controls.Add(this.label2);
            this.Ajouts.Controls.Add(this.Btn_Etudiant_Ajouter);
            this.Ajouts.Controls.Add(this.Txt_Etudiant_Nom);
            this.Ajouts.Location = new System.Drawing.Point(12, 12);
            this.Ajouts.Name = "Ajouts";
            this.Ajouts.Size = new System.Drawing.Size(417, 169);
            this.Ajouts.TabIndex = 10;
            this.Ajouts.TabStop = false;
            this.Ajouts.Text = "Ajouts";
            // 
            // Txt_Etudiant_Tel
            // 
            this.Txt_Etudiant_Tel.Location = new System.Drawing.Point(115, 102);
            this.Txt_Etudiant_Tel.Name = "Txt_Etudiant_Tel";
            this.Txt_Etudiant_Tel.Size = new System.Drawing.Size(137, 20);
            this.Txt_Etudiant_Tel.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 135);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(39, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Login :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(8, 109);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 14;
            this.label8.Text = "Telephone :";
            // 
            // Txt_Etudiant_Login
            // 
            this.Txt_Etudiant_Login.Location = new System.Drawing.Point(115, 128);
            this.Txt_Etudiant_Login.Name = "Txt_Etudiant_Login";
            this.Txt_Etudiant_Login.Size = new System.Drawing.Size(137, 20);
            this.Txt_Etudiant_Login.TabIndex = 13;
            // 
            // Txt_Etudiant_Prenom
            // 
            this.Txt_Etudiant_Prenom.Location = new System.Drawing.Point(115, 48);
            this.Txt_Etudiant_Prenom.Name = "Txt_Etudiant_Prenom";
            this.Txt_Etudiant_Prenom.Size = new System.Drawing.Size(137, 20);
            this.Txt_Etudiant_Prenom.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 81);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Email :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Prenom :";
            // 
            // Txt_Etudiant_Email
            // 
            this.Txt_Etudiant_Email.Location = new System.Drawing.Point(115, 74);
            this.Txt_Etudiant_Email.Name = "Txt_Etudiant_Email";
            this.Txt_Etudiant_Email.Size = new System.Drawing.Size(137, 20);
            this.Txt_Etudiant_Email.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Nom :";
            // 
            // Btn_Etudiant_Ajouter
            // 
            this.Btn_Etudiant_Ajouter.Location = new System.Drawing.Point(311, 71);
            this.Btn_Etudiant_Ajouter.Name = "Btn_Etudiant_Ajouter";
            this.Btn_Etudiant_Ajouter.Size = new System.Drawing.Size(75, 23);
            this.Btn_Etudiant_Ajouter.TabIndex = 0;
            this.Btn_Etudiant_Ajouter.Text = "Ajouter";
            this.Btn_Etudiant_Ajouter.UseVisualStyleBackColor = true;
            this.Btn_Etudiant_Ajouter.Click += new System.EventHandler(this.Btn_Etudiant_Ajouter_Click);
            // 
            // Txt_Etudiant_Nom
            // 
            this.Txt_Etudiant_Nom.Location = new System.Drawing.Point(115, 21);
            this.Txt_Etudiant_Nom.Name = "Txt_Etudiant_Nom";
            this.Txt_Etudiant_Nom.Size = new System.Drawing.Size(137, 20);
            this.Txt_Etudiant_Nom.TabIndex = 4;
            // 
            // EtudiantForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(454, 333);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Ajouts);
            this.Name = "EtudiantForm";
            this.Text = "Gestion Etudiant";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Ajouts.ResumeLayout(false);
            this.Ajouts.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox Txt_Etudiant_IDaction;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Btn_Etudiant_Supprimer;
        private System.Windows.Forms.Button Btn_Etudiant_Modifier;
        private System.Windows.Forms.GroupBox Ajouts;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Btn_Etudiant_Ajouter;
        private System.Windows.Forms.TextBox Txt_Etudiant_Nom;
        private System.Windows.Forms.TextBox Txt_Etudiant_Tel;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox Txt_Etudiant_Login;
        private System.Windows.Forms.TextBox Txt_Etudiant_Prenom;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox Txt_Etudiant_Email;
        private System.Windows.Forms.Button Btn_Etudiant_Lister;
    }
}